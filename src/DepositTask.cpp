#include "DepositTask.h"
#include "Common.h"

int i = 0;

DepositTask::DepositTask(MsgService* mService, Light* lA, Light* lB, Light* lC, ServoMotor* servo){
    this->mService = mService;
    this->servo = servo;
    this->lA = lA;
    this->lB = lB;
    this->lC = lC;
    depositState = START;
}

void DepositTask::init(int period, Task* initialTask){
    Task::init(period);
    this->initialTask = initialTask;
}

void DepositTask::tick(){
    switch (this->depositState)
    {
    case START:
        openDumpster();
        this->timer.init(OPENING_TIME);
        this->depositState = OPENING;
        Serial.println("start opening");
        break;
        
    case OPENING:
        Serial.println(".");
        if(this->timer.updateAndCheckTime(this->myPeriod)){
            this->depositState = DEPOSITING;
            this->timer.init(DEPOSIT_TIME);
            Serial.println("start deposit");
            this->mService->sendMsg(Msg(OPENED_MESSAGE));
        }
        break;

    case DEPOSITING:
        i++;
        if(this->timer.updateAndCheckTime(this->myPeriod)){
            closeDumpster();
            this->timer.init(CLOSING_TIME);
            this->depositState = CLOSING;
            Serial.println("start closing");
        }
        else if(this->mService->isMsgAvailable())
        {
            Msg* m = this->mService->receiveMsg();
            this->handleMsg(m);
        }
        break;

    case CLOSING:
        Serial.println(".");
        if(this->timer.updateAndCheckTime(this->myPeriod)){
            this->depositState = START;
            this->setActive(false);
            this->initialTask->setActive(true);
            lA->switchOff();
            lB->switchOff();
            lC->switchOff();
            this->mService->sendMsg(Msg(CLOSED_MESSAGE));
        }
        break;
    
    default:
        break;
    }

}

void DepositTask::handleMsg(Msg* msg){
    String m = msg->getContent();
    String mType = getMsgType(m);
    String mContent = getMsgContent(m);
    if (mType == CLOSE_TRASH)
    {
        closeDumpster();
        this->timer.init(CLOSING_TIME);
        this->depositState = CLOSING;

    }
    else if (mType == ADD_TIMER)
    {
        Serial.println("adding:" + mContent);
        this->timer.updateAndCheckTime(-mContent.toInt());
        this->mService->sendMsg(Msg(ADDITION_OK));
    }    
    else
    {
        this->mService->sendMsg(Msg("invalid message"));
    }
}

void DepositTask::openDumpster(){
    this->servo->setPosition(OPENED_POSITION);
}

void DepositTask::closeDumpster(){
    this->servo->setPosition(CLOSED_POSITION);
}

