#ifndef __AUTHTASK__
#define __AUTHTASK__
#include "MsgService.h"
#include "Task.h"

class AuthenticationTask: public Task {
public:
	AuthenticationTask(MsgService* mServiceTask, Task* regenerateTokenTask);
    void init(int period, Task* authenticatedTask);
    void tick();

private:
    MsgService* mService;
    Task* authenticatedTask;
    Task* regenerateTokenTask;
    void handleMsg(Msg* msg);
    void authenticationError();
    void authenticationOK();
};

#endif