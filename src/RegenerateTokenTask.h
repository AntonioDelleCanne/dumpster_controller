#ifndef __RGTOKEN__
#define __RGTOKEN__
#include "MsgService.h"
#include "Task.h"

class RegenerateTokenTask: public Task {
public:
    void tick();
private:
    String generateToken();
    String encryptToken(String token);
};
#endif