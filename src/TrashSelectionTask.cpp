#include "TrashSelectionTask.h"
#include "Common.h"

TrashSelectionTask::TrashSelectionTask(MsgService *mService, Light *lA, Light *lB, Light *lC)
{
    this->mService = mService;
    this->lA = lA;
    this->lB = lB;
    this->lC = lC;
    timerInitialized = false;
}


void TrashSelectionTask::init(int period, Task* initialTask, Task* trashSelectedTask){
    Task::init(period);
    this->trashSelectedTask = trashSelectedTask;
    this->initialTask = initialTask;
}

void TrashSelectionTask::tick()
{
    if(!timerInitialized){
        this->timer.init(TIMEOUT_TIME);
        timerInitialized = true;
    }
    if (this->timer.updateAndCheckTime(this->myPeriod))
    {
        timerInitialized = false;
        this->initialTask->setActive(true);
        this->setActive(false);
        lA->switchOff();
        lB->switchOff();
        lC->switchOff();
        //Serial.println(TIMEOUT_MESSAGE);
        mService->sendMsg(Msg(TIMEOUT_MESSAGE));
    } else if (this->mService->isMsgAvailable())
    {
        Msg *m = this->mService->receiveMsg();
        this->handleMsg(m);
    }
}

void TrashSelectionTask::handleMsg(Msg *msg)
{
    String m = msg->getContent();
    String mType = getMsgType(m);
    String mContent = getMsgContent(m);
    if (mType == SELECT_TRASH_TYPE)
    {
        selectTrash(mContent.charAt(0));
    }
    else
    {
        this->mService->sendMsg(Msg("invalid message"));
    }
}

void TrashSelectionTask::selectTrash(char type)
{
    Serial.println(type);
    switch (type)
    {
    case A:
        this->lA->switchOn();
        this->lB->switchOff();
        this->lC->switchOff();
        selectedType = A;
        break;
    case B:
        this->lA->switchOff();
        this->lB->switchOn();
        this->lC->switchOff();
        selectedType = B;
        break;
    case C:
        this->lA->switchOff();
        this->lB->switchOff();
        this->lC->switchOn();
        selectedType = C;
        break;

    default:
        mService->sendMsg(Msg(TRASH_SELECTION_ERROR));
        return;
    }
    timerInitialized = false;
    mService->sendMsg(Msg(TRASH_SELECTION_OK));
    this->trashSelectedTask->setActive(true);
    this->setActive(false);
}
