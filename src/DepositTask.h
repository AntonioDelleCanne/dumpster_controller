#ifndef __DEPTASK__
#define __DEPTASK__
#include "MsgService.h"
#include "Task.h"
#include "Common.h"
#include "Light.h"
#include "servo_motor.h"
#include "PeriodTimer.h"


#define OPENING_TIME 500
#define DEPOSIT_TIME 30000
#define CLOSING_TIME 500
#define OPENED_POSITION 150
#define CLOSED_POSITION 50


enum DepositState{START, OPENING, DEPOSITING, CLOSING};

class DepositTask: public Task {
public:
	DepositTask(MsgService* mService, Light* lA, Light* lB, Light* lC, ServoMotor* servo);
    void init(int pediod, Task* initialTask);
    void tick();

private:
    MsgService* mService;
    PeriodTimer timer;
    ServoMotor* servo;
    Task* initialTask;
    Light* lA;
    Light* lB;
    Light* lC;
    DepositState depositState;
    void handleMsg(Msg* msg);
    void openDumpster();
    void closeDumpster();
};

#endif