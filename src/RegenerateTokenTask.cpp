#include "RegenerateTokenTask.h"
#include "Common.h"

void RegenerateTokenTask::tick(){
    token = generateToken();
    encryptedToken = encryptToken(token);
}

String RegenerateTokenTask::generateToken(){
    int tokenLenght = 10;
    return gen_random(tokenLenght);
}

String RegenerateTokenTask::encryptToken(String token){
    int encryptionKey = 8;
    String res = String(token);
    for(int i = 0; i < token.length(); i++){
        res[i] = token[i] + encryptionKey;
    }
    return res;
}
