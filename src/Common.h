#ifndef __COMMON__
#define __COMMON__


//-------------MESSAGE CODES-----------
#define MSG_DIVIDER ':'
#define TOKEN_REQUEST "r_token"
#define TOKEN_SUBMIT "s_token"
#define SELECT_TRASH_TYPE "s_t_type"
#define CLOSE_TRASH "c_trash"
#define ADD_TIMER "add_t"
#define SEND_TOKEN "token:"
#define SEND_SERVER_NAME "server:http://225a5c1b.ngrok.io/Progetto-03/src/sd-service/script/"

//---------------MESSAGE RESPONSES-------------------
#define AUTH_ERROR "auth:error"
#define AUTH_OK "auth:ok"
#define TRASH_SELECTION_OK "s_t_type:ok"
#define TRASH_SELECTION_ERROR "s_t_type:error"
#define TIMEOUT_MESSAGE "error:timeout"
#define OPENED_MESSAGE "trash:opened"
#define CLOSED_MESSAGE "trash:closed"
#define ADDITION_OK "add_t:ok"


enum TrashType {A='a', B='b', C='c'};

extern TrashType selectedType;
extern String token;
extern String encryptedToken;
extern int n;

extern String getMsgType(String m);

extern String getMsgContent(String m);

extern String gen_random(const int len);

#endif


