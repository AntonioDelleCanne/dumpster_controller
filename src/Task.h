#ifndef __TASK__
#define __TASK__

class Task {
	int timeElapsed;
	bool active;

public:

	Task() {
		this->active = false;
		this->timeElapsed = 0;
		this->myPeriod = 0;
	}

	virtual void init(int period) {
		myPeriod = period;
		timeElapsed = 0;
		this->active = false;
	}

	virtual void tick() = 0;

	bool updateAndCheckTime(int basePeriod) {
		timeElapsed += basePeriod;
		if (timeElapsed >= myPeriod) {
			timeElapsed = 0;
			return true;
		}
		else {
			return false;
		}
	}

	bool isActive() {
		return active;
	}

	void setActive(bool active) {
		this->active = active;
	}

protected:
	int myPeriod;

};

#endif
