#ifndef __TSELTASK__
#define __TSELTASK__
#include "MsgService.h"
#include "Task.h"
#include "Common.h"
#include "Light.h"
#include "PeriodTimer.h"

#define TIMEOUT_TIME 10000

class TrashSelectionTask: public Task {
public:
	TrashSelectionTask(MsgService* mService, Light* lA, Light* lB, Light* lC);
    void init(int period, Task* initialTask, Task* trashSelectedTask);
    //void setActive(bool active);
    void tick();

private:
    MsgService* mService;
    Task* trashSelectedTask;
    Task* initialTask;
    PeriodTimer timer;
    Light* lA;
    Light* lB;
    Light* lC;
    bool timerInitialized;
    void handleMsg(Msg* msg);
    void selectTrash(char type);
};

#endif