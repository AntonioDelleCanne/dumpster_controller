#include "servo_motor_impl.h"
#include "Arduino.h"
#include "MsgService.h"

ServoMotorImpl::ServoMotorImpl(int pin){
  this->pin = pin;  
} 

void ServoMotorImpl::on(){
  motor.attach(pin);    
}

void ServoMotorImpl::setPosition(int angle){
  float coeff = (MAX_PULSE_WIDTH-MIN_PULSE_WIDTH)/180;
  motor.write(MIN_PULSE_WIDTH + angle*coeff);              
}

void ServoMotorImpl::off(){
  motor.detach();    
}
