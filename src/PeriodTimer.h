#ifndef __PERTIMER__
#define __PERTIMER__

class PeriodTimer {
	int myPeriod;
	int timeElapsed;

public:

	PeriodTimer() {
		this->timeElapsed = 0;
		this->myPeriod = 0;
	}

	virtual void init(int period) {
		myPeriod = period;
		timeElapsed = 0;
	}

	bool updateAndCheckTime(int basePeriod) {
		timeElapsed += basePeriod;
		if (timeElapsed >= myPeriod) {
			return true;
		}
		else {
			return false;
		}
	}
};

#endif
