#include <Arduino.h>
#include "Common.h"
#include "Scheduler.h"
#include "MsgService.h"
#include "Led.h"
#include "AuthenticationTask.h"
#include "RegenerateTokenTask.h"
#include "TrashSelectionTask.h"
#include "DepositTask.h"
#include "servo_motor_impl.h"

//-------------------PINS----------------------

#define RX_BT_PIN 2
#define TX_BT_PIN 3
#define SERVO_PIN 12
#define LED_A_PIN 7
#define LED_B_PIN 8
#define LED_C_PIN 9

//----------------PERIODS----------------------------
#define ATUH_TASK_PERIOD 200
#define SELECTION_TASK_PERIOD 200
#define DEPOSIT_TASK_PERIOD 200
#define TOKEN_REGEN_PERIOD 4000
#define SCHEDULER_PERIOD 200

//----------------GLOBAL VARIABLES-------------------
TrashType selectedType = A;
String token = "abcd1234";
String encryptedToken = "bcde2345";

//-----------LOCAL VARIABLES---------------------------

Scheduler sched;
MsgService *btMsgService;
Task *regenerateTokenTask;
AuthenticationTask *authTask;
TrashSelectionTask *trashSelectionTask;
DepositTask *depositTask;

ServoMotorImpl *servo;
Light *ledA;
Light *ledB;
Light *ledC;

void setup()
{
  Serial.begin(9600);
  btMsgService = new MsgService(RX_BT_PIN, TX_BT_PIN);
  ledA = new Led(LED_A_PIN);
  ledB = new Led(LED_B_PIN);
  ledC = new Led(LED_C_PIN);
  ledA->switchOff();
  ledB->switchOff();
  ledC->switchOff();
  servo = new ServoMotorImpl(SERVO_PIN);
  servo->on();
  servo->setPosition(CLOSED_POSITION);
  regenerateTokenTask = new RegenerateTokenTask();
  authTask = new AuthenticationTask(btMsgService, regenerateTokenTask);
  trashSelectionTask = new TrashSelectionTask(btMsgService, ledA, ledB, ledC);
  depositTask = new DepositTask(btMsgService, ledA, ledB, ledC, servo);

  btMsgService->init();
  regenerateTokenTask->init(TOKEN_REGEN_PERIOD);
  depositTask->init(DEPOSIT_TASK_PERIOD, authTask);
  trashSelectionTask->init(SELECTION_TASK_PERIOD, authTask, depositTask);
  authTask->init(ATUH_TASK_PERIOD, trashSelectionTask);
  sched.init(SCHEDULER_PERIOD);

  trashSelectionTask->setActive(false);
  depositTask->setActive(false);
  regenerateTokenTask->setActive(true);
  authTask->setActive(true);
  regenerateTokenTask->tick();

  sched.addTask(authTask);
  sched.addTask(trashSelectionTask);
  sched.addTask(depositTask);
  sched.addTask(regenerateTokenTask);
  delay(200);
  Serial.println("Starting...");
}

void loop()
{
  sched.schedule();
}

//-------------------FUNCTIONS----------------
String getMsgType(String m)
{
  return m.substring(0, m.indexOf(MSG_DIVIDER));
}

String getMsgContent(String m)
{
  return m.substring(m.indexOf(MSG_DIVIDER) + 1);
}

String gen_random(const int len) {
    static const char alphanum[] =
        "0123456789"
        "ABCDEFGHIJKLMNOPQRSTUVWXYZ"
        "abcdefghijklmnopqrstuvwxyz";
    char res[len+1];
    for (int i = 0; i < len; ++i) {
        res[i] = alphanum[rand() % (sizeof(alphanum) - 1)];
    }
    res[len] = '\0';
    return String(res);
}