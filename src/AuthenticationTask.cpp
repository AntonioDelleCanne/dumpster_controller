#include "AuthenticationTask.h"
#include "Common.h"


AuthenticationTask::AuthenticationTask(MsgService* mService, Task* regenerateTokenTask){
    this->mService = mService;
    this->regenerateTokenTask = regenerateTokenTask;
}

void AuthenticationTask::init(int period , Task* authenticatedTask){
    Task::init(period);
    this->authenticatedTask = authenticatedTask;
}

void AuthenticationTask::tick(){
    if(this->mService->isMsgAvailable()){
        
        Msg* m = this->mService->receiveMsg();
        this->handleMsg(m);
    }
}

void AuthenticationTask::handleMsg(Msg* msg){
    String m = msg->getContent();
    String mType = getMsgType(m);
    String mContent = getMsgContent(m);
    if (mType == TOKEN_REQUEST)
    {
        mService->sendMsg(Msg(SEND_TOKEN + encryptedToken));
        mService->sendMsg(Msg(SEND_SERVER_NAME));
    }
    else if (mType == TOKEN_SUBMIT)
    {
        if(mContent == token){
            authenticationOK();
        } else {
            authenticationError();
        }
    }
    else
    {
        this->mService->sendMsg(Msg("invalid message"));
    }
    
}

void AuthenticationTask::authenticationOK(){
    mService->sendMsg(Msg(AUTH_OK));
    delay(200);
    this->regenerateTokenTask->setActive(false);
    this->setActive(false);
    this->authenticatedTask->setActive(true);
}

void AuthenticationTask::authenticationError(){
    this->mService->sendMsg(Msg(AUTH_ERROR));
    //this->mService->sendMsg(Msg(token));
}

